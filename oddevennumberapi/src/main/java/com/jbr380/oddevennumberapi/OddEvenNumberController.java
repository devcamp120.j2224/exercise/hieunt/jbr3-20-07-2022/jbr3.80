package com.jbr380.oddevennumberapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
public class OddEvenNumberController {
    @CrossOrigin
    @GetMapping("/checknumber")
    public String checkEvenOddNumber(@RequestParam(required=true) int number) {
        String result = null;
        if (number % 2 == 0){
            result = "Số đã cho là số chẵn";
        }
        else {
            result = "Số đã cho là số lẻ";
        }
        return result;
    }
    
}
