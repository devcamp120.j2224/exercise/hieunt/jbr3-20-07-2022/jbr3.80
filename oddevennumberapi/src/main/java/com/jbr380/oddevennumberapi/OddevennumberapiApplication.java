package com.jbr380.oddevennumberapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OddevennumberapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OddevennumberapiApplication.class, args);
	}

}
